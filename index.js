'use strict';

const fs = require('fs');
const path = require('path');
const stylelint = require('stylelint');
const dir = '/var/www/creatives/creatives/pigu.lt_v5/300x250/';
const CLIEngine = require('eslint').CLIEngine;
const htmllint = require('htmllint');
const htmlConfFile = 'configs/htmllintrc.json';

const cli = new CLIEngine({
  envs: ['browser', 'mocha'],
  useEslintrc: false,
  configFile: 'configs/eslintrc.json'
});

const fn = (data) => {
  if (true === data.errored) {
    console.log('');
    console.log('style.css warnings ');
    const output =  JSON.parse(data.output);
    const warnings =  output[0].warnings;
    for (const i in warnings) {
      console.log(warnings[i].severity + ' ' + warnings[i].line + '-' +
        warnings[i].column + ' "' + warnings[i].text + '"');
    }
  } else {
    console.log('Clean style.css');
  }
};

const fn2 = (err) => {
  // do things with err e.g.
  console.error(err.stack);
};

const fn3 = (content, error, html) => {
  const opts = JSON.parse(content);
  htmllint(html.toString(), opts).then((out) => { console.log(out); });
};

const fn4 = (content, result) => {
  const htmlFile = dir + 'test.html';
  if ('undefined' !== typeof result && result !== false) {
    fs.readFile(htmlFile, 'utf8', fn3.bind(null, content));
  } else {
    console.log(' No test.html file ');
  }
};

const fn5 = (errorConf, content) => {
  const htmlFile = dir + 'test.html';
  if (null !== errorConf) {
    console.error(errorConf);
  } else {
    fs.exists(htmlFile, fn4.bind(null, content));
  }
};

const fn6 = (filename, result) => {
  if ('undefined' !== typeof result && result !== false) {
    fs.readFile(filename, 'utf8', fn5);
  } else {
    console.log(' No config file ');
  }
};

// lint myfile.js and all files in lib/
const report = cli.executeOnFiles([ dir + 'code.js' ]);

if (0 < report.errorCount) {
  console.log('code.js warnings ');
  const messages = report.results[0].messages;
  for (const i in messages) {
    console.log(' ' + messages[i].line + '-' + messages[i].column + ' "' +
      messages[i].message + '" //' + messages[i].ruleId + '');
  }
  //console.log('messages ', JSON.stringify(report.results[0].messages));
}
// console.log('report ', JSON.stringify(report));
// galima visa report išsaugoti json byloje
stylelint.lint({
  configFile: 'configs/stylelintrc.json',
  configBasedir: path.join(__dirname, 'configs'),
  files: dir + 'style.css'
}).then(fn).catch(fn2);

// lint html/
fs.exists(htmlConfFile, fn6.bind(null, htmlConfFile));

